package ardelean.ana.maria.lab3.ex1;

public class Robot {
    int x;

    public Robot() {
        x = 1;
    }

    public Robot(int x) {
        this.x = x;
    }

    public void change(int k) {
        if (k >= 1)
            this.x = this.x + k;
    }

    public String toString() {
        return "Position{" + "x=" + x + '}';
    }


}

