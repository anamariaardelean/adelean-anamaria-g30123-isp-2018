package ardelean.ana.maria.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(4, 54);
        p1.setXY(2, 2);
        p2.setX(8);
        p2.setY(8);
        System.out.println(p1.distance(2, 2));
        System.out.println(p1.distance(p2));
    }
}
