package ardelean.ana.maria.lab5.ex4;

import ardelean.ana.maria.lab5.ex3.LightSensor;
import ardelean.ana.maria.lab5.ex3.TemperatureSensor;

import java.util.concurrent.TimeUnit;

class Controller1 {
    private static Controller1 single_instance = null;

    private Controller1(){
    }

    public void control1() throws InterruptedException {
        short count = 0;
        while (count < 20) {
            TimeUnit.SECONDS.sleep(1);
            System.out.println("TempSensor:" + TemperatureSensor.getTempSensor() + " si LightSensor:" + LightSensor.getKightSensor());
            count++;
        }
    }

    public static Controller1 getInstance() {
        if (single_instance == null) single_instance = new Controller1();
        return single_instance;
    }

}

class Main{
    public static void main(String[] args) throws InterruptedException {
        Controller1 c2 = Controller1.getInstance();
    }
}
