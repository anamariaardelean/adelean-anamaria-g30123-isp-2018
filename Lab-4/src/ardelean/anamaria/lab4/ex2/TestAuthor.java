package ardelean.anamaria.lab4.ex2;

public class TestAuthor {
    public static void main(String[] args) {
        Author A = new Author("Ana-Maria", "ardelean_ana_maria@yahoo.com", 'f');
        System.out.println("The name of the author is " + A.getName());
        System.out.println("The email of the author is " + A.getEmail());
        System.out.println("The gender of the author is " + A.getGender());
        System.out.println(A.toString());
    }
}
