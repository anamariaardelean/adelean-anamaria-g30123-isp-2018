package ardelean.anamaria.lab4.ex3;
import ardelean.anamaria.lab4.ex2.Author;
public class TestBook {
    public static void main(String[] args) {
        Author A = new Author("Ardelean", "ardelean_ana_maria@yahoo.com", 'f');
        Book B1 = new Book("Ardelean","ardelean_ana_maria@yahoo.com",'f',"The Intelligent Investor ",A,250);
        Book B2 = new Book("Iacob","ancaiacob_98@gmail",'f',"Think & Grow Rich",A,100,6);
        System.out.println("The author of B1 is "+ B1.getAuthor());
        System.out.println("The price of the book 'The Book' is "+ B1.getPrice());
        B1.setPrice(200);
        System.out.println("The new price of the book 'The Intelligent Investor ' is "+ B1.getPrice());
        System.out.println("The quantity of the book 'Think & Grow Rich' is "+ B2.getQtyInStock());
    }
}
