package ardelean.anamaria.lab4.ex1;

public class TestCircle {
    public static void main(String[] args) {
        Circle C = new Circle(3);
        System.out.println("The radius of the circle is " + C.getRadius());
        System.out.println("The area of the circle is " + C.getArea());

    }
}
