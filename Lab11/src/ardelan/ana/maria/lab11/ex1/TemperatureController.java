package ardelan.ana.maria.lab11.ex1;

public class TemperatureController  {
    Sensor t;
    Display tview;
    public TemperatureController(Sensor t, Display tview){
        t.addObserver(tview);
        this.t = t;
        this.tview = tview;
    }
}