package ardelan.ana.maria.lab11.ex1;

import javax.swing.*;
import java.awt.*;

public class TemperatureApp extends JFrame {

    TemperatureApp(Display tview){
        setLayout(new BorderLayout());
        add(tview,BorderLayout.NORTH);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        Sensor t = new Sensor();
        t.start();
        Display tview = new Display();
        TemperatureController tcontroler = new TemperatureController(t,tview);
        new TemperatureApp(tview);
    }
}